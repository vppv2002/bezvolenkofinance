package com.company.model;

import com.company.Interface.Credit;

public class CreditService extends Organization implements Credit {
    private float creditPercent;
    private int creditLimit;

    public CreditService(String name, String address, float creditPercent, int creditLimit) {
        super(name, address);
        this.creditLimit = creditLimit;
        this.creditPercent = creditPercent;
    }

    @Override
    public float takeCredit(int money) {
        if (money < creditLimit) {
            return money * (1 + creditPercent);
        }
        return 0;
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s; Адрес: %s; Годовой процент на кредит: %.0f", super.name, super.address, creditPercent * 100));
    }
}
