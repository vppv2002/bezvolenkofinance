package com.company.model;

public abstract class Organization {

    protected String name;
    protected String address;

    public Organization(String name, String address) {
        this.name = name;
        this.address = address;
    }

    abstract public void showInfo();
}
