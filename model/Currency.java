package com.company.model;


public class Currency {
    private String name;
    private float pokupka;
    private float prodaga;

    public Currency(String name, float pokupka, float prodaga) {
        this.name = name;
        this.pokupka = pokupka;
        this.prodaga = prodaga;
    }

    public String getName() {
        return name;
    }

    public float getPokupka() {
        return pokupka;
    }

    public float getProdaga() {
        return prodaga;
    }
}
