package com.company.model;

import com.company.Interface.Transfer;

public class TransferService extends Organization implements Transfer {
    private float transferProcent;

    public TransferService(String name, String address, float transferProcent) {
        super(name, address);
        this.transferProcent = transferProcent;
    }

    @Override
    public float transfer(int money) {
        return money * (1 + transferProcent);
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s; Адрес: %s; Процент перевода: %.0f;", super.name, super.address, transferProcent));
    }

}
