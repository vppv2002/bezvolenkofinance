package com.company.model;

import com.company.Interface.Deposit;

public class DepositService extends Organization implements Deposit {
    private int yearOfFoundation;
    private float depositPercent;
    private int depositLimitOfMonths;

    public DepositService(String name, String address, int yearOfFoundation, float depositPercent, int depositLimitOfMonths) {
        super(name, address);
        this.yearOfFoundation = yearOfFoundation;
        this.depositPercent = depositPercent;
        this.depositLimitOfMonths = depositLimitOfMonths;
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s; Адрес: %s; Год открытия: %d; Годовой процент на депозит: %.0f", super.name, super.address, yearOfFoundation, depositPercent * 100));
    }

    @Override
    public float takeDeposit(int money, int numberOfMonths) {
        if (numberOfMonths >= depositLimitOfMonths) {
            return money + (money * depositPercent * numberOfMonths / 12);
        }
        return 0;
    }
}
