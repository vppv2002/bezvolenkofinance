package com.company.model;

import com.company.Interface.ChangeMoney;

import java.util.List;

public class Exchanger extends Organization implements ChangeMoney {
    private List<Currency> rate;

    public Exchanger(String name, String address, List<Currency> rate) {
        super(name, address);
        this.rate= rate;
    }

    @Override
    public float sell(int money, String currency) {
        for (Currency rates : rate) {
            if (rates.getName().equalsIgnoreCase(currency)) {
                return money * rates.getProdaga();
            }
        }
        return 0;
    }

    @Override
    public float buy(int money, String currency) {
        for (Currency rates : rate) {
            if (rates.getName().equalsIgnoreCase(currency)) {
                return money / rates.getProdaga();
            }
        }
        return 0;
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s; Адрес: %s;", super.name, super.address));
    }
}
