package com.company.model;

import com.company.Interface.ChangeMoney;
import com.company.Interface.Credit;
import com.company.Interface.Deposit;
import com.company.Interface.Transfer;

import java.util.List;

public class Bank extends Organization implements ChangeMoney, Transfer, Credit, Deposit {
    private int changeLimit = 150_000;
    private int changeComission = 15;
    private int yearOfGettingLisence;
    private List<Currency> rates;
    private float transferProcent = 0.01f;
    private int transferCommision = 25;
    private float creditPercent = 0.25f;
    private int creditLimit = 200000;
    private float depositPercent = 0.12f;
    private int depositLimitOfMonths = 12;

    public Bank(String name, String address, int yearOfGettingLisence, List<Currency> rates) {
        super(name, address);
        this.yearOfGettingLisence = yearOfGettingLisence;
        this.rates = rates;

    }

    @Override
    public float sell(int money, String currency) {
        for (Currency rates : rates) {
            if (rates.getName().equalsIgnoreCase(currency)) {
                float cash = money * rates.getProdaga();
                if (moreThanChangeLimit(cash, changeLimit)) {
                    return (cash - changeComission);
                }
            }
        }
        return 0;
    }

    @Override
    public float buy(int money, String currency) {
        for (Currency rates : rates) {
            if (rates.getName().equalsIgnoreCase(currency)) {
                if (moreThanChangeLimit(money, changeLimit)) {
                    return (money - changeComission) / rates.getPokupka();
                }
            }
        }
        return 0;
    }

    public boolean moreThanChangeLimit(float cash, int limit) {
        return cash < limit;
    }

    @Override
    public float transfer(int money) {
        return money * (1 + transferProcent) + transferCommision;
    }

    @Override
    public float takeCredit(int money) {
        if (money < creditLimit) {
            return money * (1 + creditPercent);
        }
        return 0;
    }

    @Override
    public float takeDeposit(int money, int numberOfMonths) {
        if (numberOfMonths <= depositLimitOfMonths) {
            return money + (money * depositPercent * numberOfMonths / 12);
        }
        return 0;
    }

    @Override
    public void showInfo() {
        System.out.println(String.format("Название: %s; Адрес: %s; Год получения лицензии: %d;", super.name, super.address, yearOfGettingLisence));
        System.out.println("Банк предоставляет услуги: ");
        System.out.println(String.format("Перевод денежных средств с процентом: %.0f", transferProcent * 100));
        System.out.println(String.format("Годовой процент на кредит: %.0f", creditPercent * 100));
        System.out.println(String.format("Годовой процент на депозит: %.0f", depositPercent * 100));

    }
}