package com.company;


import com.company.data.Generator;
import com.company.model.Organization;
import com.company.Interface.Transfer;

import java.util.List;
import java.util.Scanner;

public class TransferManager {
    private final static int BIG_INT = Integer.MAX_VALUE;

    public static void run() {
        List<Organization> organizations = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите сумму для перевода: ");
        int inputMoney = Integer.parseInt(scan.nextLine());

        float finalNumberOfMoney = BIG_INT;
        int index = 0;
        for (int i = 0; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof Transfer) {
                Transfer trans = (Transfer) organizations.get(i);
                float numberOfMoneyInCycle = trans.transfer(inputMoney);
                if (numberOfMoneyInCycle < finalNumberOfMoney) {
                    finalNumberOfMoney = numberOfMoneyInCycle;
                    index = i;
                }
            }
        }
        System.out.println(String.format("Сумма вместе с комиссией = %.2f", finalNumberOfMoney));
        System.out.println("");
        System.out.println("Лучшая организация денежнего перевода: ");
        organizations.get(index).showInfo();

    }
}
