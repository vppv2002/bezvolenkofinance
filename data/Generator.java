package com.company.data;

import com.company.model.*;

import java.util.ArrayList;
import java.util.List;

public final class Generator {
    private Generator() {
    }

    public static List<Organization> generate() {
        List<Organization> organizations = new ArrayList<>();
        {
            List<Currency> currencies = new ArrayList<>();
            currencies.add(new Currency("usd", 27f, 27.5f));
            currencies.add(new Currency("eur", 29.2f, 29.8f));
            currencies.add(new Currency("rub", 0.33f, 0.38f));
            organizations.add(new Bank("ПриватБанк", "ул. Пушкинская 17", 2000, currencies));
        }
        {
            List<Currency> currencies = new ArrayList<>();
            currencies.add(new Currency("usd", 26.75f, 27.45f));
            currencies.add(new Currency("eur", 29.5f, 30f));
            organizations.add(new Exchanger(" Money24", "ул. Гоголя 6", currencies));
        }

        organizations.add(new TransferService("Почта", "проспект Академика Курчатова 28", 0.02f));

        organizations.add(new TransferService("Сервис онлайн переводов", "perevod.com", 0.05f));

        organizations.add(new CreditService("Lombard ", "ул. Сумская 24", 0.4f, 50000));

        organizations.add(new CreditService("Credit Cafe ", "ул. Сумская 102", 2f, 4000));

        organizations.add(new CreditService("Кредитный союз ", "ул. Академика Павлова 77", 0.2f, 100000));

        organizations.add(new DepositService("Инвестиционный Фонд", "ул. Мироносицкая 36", 1999, 0.15f, 12));

        return organizations;
    }
}