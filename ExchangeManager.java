package com.company;

import com.company.data.Generator;
import com.company.Interface.ChangeMoney;
import com.company.model.Organization;

import java.util.List;
import java.util.Scanner;

public class ExchangeManager {
    public static void run() {
        List<Organization> organizations = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("С какой валютой вы хотите провести операцию (usd, eur, rub): ");
        String currency = scan.nextLine();

        System.out.println("Введите операцию (Продажа или покупка):  ");
        String operation = scan.nextLine();

        System.out.println("Введите сумму: ");
        int money = Integer.parseInt(scan.nextLine());


        float numberOfMoneyInCycle = 0;
        float finalNumberOfMoney = 0;
        int indexOfMin = 0;
        for (int i = 0; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof ChangeMoney) {
                ChangeMoney changeMoney = (ChangeMoney) organizations.get(i);
                if (operation.equalsIgnoreCase("Продажа")) {
                    numberOfMoneyInCycle = changeMoney.sell(money, currency);
                } else {
                    numberOfMoneyInCycle = changeMoney.buy(money, currency);
                }
                if (numberOfMoneyInCycle > finalNumberOfMoney) {
                    finalNumberOfMoney = numberOfMoneyInCycle;
                    indexOfMin = i;
                }
            }
        }
        System.out.println(String.format("Полученная сумма: %.2f", finalNumberOfMoney));
        System.out.println("");
        System.out.println("Лучшая организация для этой операции:");
        organizations.get(indexOfMin).showInfo();

    }
}
