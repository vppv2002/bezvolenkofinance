package com.company;


import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите номер операции(1-обмен валют; 2-перевод денежных средств; 3-кредит; 4-депозит): ");
        int numberOfOperation = Integer.parseInt(scan.nextLine());

        if (numberOfOperation == 1) {
            new ExchangeManager().run();
        } else if (numberOfOperation == 2) {
            new TransferManager().run();
        } else if (numberOfOperation == 3) {
            new CreditManager().run();
        } else if (numberOfOperation == 4) {
            new DepositManager().run();
        }
    }
}
