package com.company.Interface;

public interface Credit {
    float takeCredit(int money);
}
