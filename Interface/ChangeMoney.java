package com.company.Interface;

public interface ChangeMoney {
    float sell(int money, String currency);

    float buy(int money, String currency);
}
