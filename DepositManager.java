package com.company;

import com.company.Interface.Credit;
import com.company.Interface.Deposit;
import com.company.data.Generator;
import com.company.model.Organization;

import java.util.List;
import java.util.Scanner;

public class DepositManager {
    private final static int BIG_INT = Integer.MAX_VALUE;

    public static void run() {
        List<Organization> organizations = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите сумму которую хотите инвестировать: ");
        int money = Integer.parseInt(scan.nextLine());

        System.out.println("Введите количество месяцев, на которое хотите инвестировать: ");
        int numberOfMonths = Integer.parseInt(scan.nextLine());

        float finalNumberOfMoney = 0;
        int indexOfMin = 0;
        for (int i = 0; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof Deposit) {
                Deposit deposit = (Deposit) organizations.get(i);
                float numberOfMoneyInCycle = deposit.takeDeposit(money, numberOfMonths);
                if (numberOfMoneyInCycle > finalNumberOfMoney) {
                    finalNumberOfMoney = numberOfMoneyInCycle;
                    indexOfMin = i;
                }
            }
        }
        System.out.println(String.format("Сумма которую вы получите через %d месяцев: %.2f", numberOfMonths, finalNumberOfMoney));
        System.out.println("");
        System.out.println("Лучшая организация для этой операции:");
        organizations.get(indexOfMin).showInfo();

    }
}
