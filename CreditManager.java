package com.company;

import com.company.Interface.Credit;
import com.company.data.Generator;
import com.company.model.Organization;

import java.util.List;
import java.util.Scanner;

public class CreditManager {
    private final static int BIG_INT = Integer.MAX_VALUE;

    public static void run() {
        List<Organization> organizations = Generator.generate();
        Scanner scan = new Scanner(System.in);

        System.out.println("Введите сумму которую хотите взять в кредит: ");
        int money = Integer.parseInt(scan.nextLine());

        float finalNumberOfMoney = BIG_INT;
        int indexOfMin = 0;
        for (int i = 0; i < organizations.size(); i++) {
            if (organizations.get(i) instanceof Credit) {
                Credit credit = (Credit) organizations.get(i);
                float numberOfMoneyInCycle = credit.takeCredit(money);
                if (numberOfMoneyInCycle < finalNumberOfMoney && numberOfMoneyInCycle != 0) {
                    finalNumberOfMoney = numberOfMoneyInCycle;
                    indexOfMin = i;
                }
            }
        }
        System.out.println(String.format("Сумма которую нужно будет отдать через 12 месяцев: %.2f", finalNumberOfMoney));
        System.out.println("");
        System.out.println("Лучшая организация для этой операции:");
        organizations.get(indexOfMin).showInfo();

    }
}
